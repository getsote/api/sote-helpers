<?php

namespace Tests;

use HELPERS\ContentHelper;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use Monolog\Processor\UidProcessor;

class ContentHelperTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;
    /**
     * @var "Slim\Http\RequestMonolog\Logger" $logger The instance of the Logger created at startup.
     */
    protected $objLogger;

    protected function _before()
    {
        // Start Logger
        $this->objLogger = new Logger('sote-helper');
        $this->objLogger->pushProcessor(new UidProcessor());
        $this->objLogger->pushHandler(new StreamHandler('php://stdout',
                                                        'DEBUG'));
    }

    protected function _after()
    {
    }

    // tests
    public function test0010ContentHelperInstance()
    {
        $myContentHelper = $this->getContentHelperInstance();
        $this->assertTrue(is_object($myContentHelper));
    }

    public function test0020ContainsSpecialCharacters()
    {
        $myContentHelper = $this->getContentHelperInstance();
        $result          = $myContentHelper->containsSpecialCharacters('Scott',
                                                                       'ScottValue');
        $this->assertTrue($result['errCode'] == 0);
    }

    public function test0030ContainsSpecialCharactersExclamation()
    {
        $myContentHelper = $this->getContentHelperInstance();
        $result          = $myContentHelper->containsSpecialCharacters('Scott',
                                                                       '!');
        $this->assertTrue($result['errCode'] == 400060);
    }

    public function test0040ContainsSpecialCharactersDoubleQoute()
    {
        $myContentHelper = $this->getContentHelperInstance();
        $result          = $myContentHelper->containsSpecialCharacters('Scott',
                                                                       '"');
        $this->assertTrue($result['errCode'] == 400060);
    }

    public function test0050ContainsSpecialCharactersHash()
    {
        $myContentHelper = $this->getContentHelperInstance();
        $result          = $myContentHelper->containsSpecialCharacters('Scott',
                                                                       '#');
        $this->assertTrue($result['errCode'] == 400060);
    }

    public function test0060ContainsSpecialCharactersDollarSign()
    {
        $myContentHelper = $this->getContentHelperInstance();
        $result          = $myContentHelper->containsSpecialCharacters('Scott',
                                                                       '$');
        $this->assertTrue($result['errCode'] == 400060);
    }

    public function test0070ContainsSpecialCharactersPercent()
    {
        $myContentHelper = $this->getContentHelperInstance();
        $result          = $myContentHelper->containsSpecialCharacters('Scott',
                                                                       '%');
        $this->assertTrue($result['errCode'] == 400060);
    }

    public function test0080ContainsSpecialCharactersAmpersand()
    {
        $myContentHelper = $this->getContentHelperInstance();
        $result          = $myContentHelper->containsSpecialCharacters('Scott',
                                                                       '&');
        $this->assertTrue($result['errCode'] == 400060);
    }

    public function test0090ContainsSpecialCharactersSingleQoute()
    {
        $myContentHelper = $this->getContentHelperInstance();
        $result          = $myContentHelper->containsSpecialCharacters('Scott',
                                                                       "'");
        $this->assertTrue($result['errCode'] == 400060);
    }

    public function test0100ContainsSpecialCharactersLeftParen()
    {
        $myContentHelper = $this->getContentHelperInstance();
        $result          = $myContentHelper->containsSpecialCharacters('Scott',
                                                                       '(');
        $this->assertTrue($result['errCode'] == 400060);
    }

    public function test0110ContainsSpecialCharactersRightParen()
    {
        $myContentHelper = $this->getContentHelperInstance();
        $result          = $myContentHelper->containsSpecialCharacters('Scott',
                                                                       ')');
        $this->assertTrue($result['errCode'] == 400060);
    }

    public function test0120ContainsSpecialCharactersAsterick()
    {
        $myContentHelper = $this->getContentHelperInstance();
        $result          = $myContentHelper->containsSpecialCharacters('Scott',
                                                                       '*');
        $this->assertTrue($result['errCode'] == 400060);
    }

    public function test0130ContainsSpecialCharactersPlus()
    {
        $myContentHelper = $this->getContentHelperInstance();
        $result          = $myContentHelper->containsSpecialCharacters('Scott',
                                                                       '+');
        $this->assertTrue($result['errCode'] == 400060);
    }

    public function test0140ContainsSpecialCharactersComma()
    {
        $myContentHelper = $this->getContentHelperInstance();
        $result          = $myContentHelper->containsSpecialCharacters('Scott',
                                                                       ',');
        $this->assertTrue($result['errCode'] == 400060);
    }

    public function test0150ContainsSpecialCharactersMinus()
    {
        $myContentHelper = $this->getContentHelperInstance();
        $result          = $myContentHelper->containsSpecialCharacters('Scott',
                                                                       '-');
        $this->assertTrue($result['errCode'] == 400060);
    }

    public function test0160ContainsSpecialCharactersPeriod()
    {
        $myContentHelper = $this->getContentHelperInstance();
        $result          = $myContentHelper->containsSpecialCharacters('Scott',
                                                                       '.');
        $this->assertTrue($result['errCode'] == 400060);
    }

    public function test0170ContainsSpecialCharactersColon()
    {
        $myContentHelper = $this->getContentHelperInstance();
        $result          = $myContentHelper->containsSpecialCharacters('Scott',
                                                                       ':');
        $this->assertTrue($result['errCode'] == 400060);
    }

    public function test0180ContainsSpecialCharactersSemiColon()
    {
        $myContentHelper = $this->getContentHelperInstance();
        $result          = $myContentHelper->containsSpecialCharacters('Scott',
                                                                       ';');
        $this->assertTrue($result['errCode'] == 400060);
    }

    public function test0190ContainsSpecialCharactersLessThan()
    {
        $myContentHelper = $this->getContentHelperInstance();
        $result          = $myContentHelper->containsSpecialCharacters('Scott',
                                                                       '<');
        $this->assertTrue($result['errCode'] == 400060);
    }

    public function test0200ContainsSpecialCharactersEqual()
    {
        $myContentHelper = $this->getContentHelperInstance();
        $result          = $myContentHelper->containsSpecialCharacters('Scott',
                                                                       '=');
        $this->assertTrue($result['errCode'] == 400060);
    }

    public function test0210ContainsSpecialCharactersGreaterThan()
    {
        $myContentHelper = $this->getContentHelperInstance();
        $result          = $myContentHelper->containsSpecialCharacters('Scott',
                                                                       '>');
        $this->assertTrue($result['errCode'] == 400060);
    }

    public function test0220ContainsSpecialCharactersQuestion()
    {
        $myContentHelper = $this->getContentHelperInstance();
        $result          = $myContentHelper->containsSpecialCharacters('Scott',
                                                                       '?');
        $this->assertTrue($result['errCode'] == 400060);
    }

    public function test0230ContainsSpecialCharactersAtsign()
    {
        $myContentHelper = $this->getContentHelperInstance();
        $result          = $myContentHelper->containsSpecialCharacters('Scott',
                                                                       '@');
        $this->assertTrue($result['errCode'] == 400060);
    }

    public function test0240ContainsSpecialCharactersLeftBracket()
    {
        $myContentHelper = $this->getContentHelperInstance();
        $result          = $myContentHelper->containsSpecialCharacters('Scott',
                                                                       '[');
        $this->assertTrue($result['errCode'] == 400060);
    }

    public function test0250ContainsSpecialCharactersBackSlash()
    {
        $myContentHelper = $this->getContentHelperInstance();
        $result          = $myContentHelper->containsSpecialCharacters('Scott',
                                                                       '\\');
        $this->assertTrue($result['errCode'] == 400060);
    }

    public function test0260ContainsSpecialCharactersRightBracket()
    {
        $myContentHelper = $this->getContentHelperInstance();
        $result          = $myContentHelper->containsSpecialCharacters('Scott',
                                                                       ']');
        $this->assertTrue($result['errCode'] == 400060);
    }

    public function test0270ContainsSpecialCharactersCaret()
    {
        $myContentHelper = $this->getContentHelperInstance();
        $result          = $myContentHelper->containsSpecialCharacters('Scott',
                                                                       '^');
        $this->assertTrue($result['errCode'] == 400060);
    }

    public function test0280ContainsSpecialCharactersUnderscore()
    {
        $myContentHelper = $this->getContentHelperInstance();
        $result          = $myContentHelper->containsSpecialCharacters('Scott',
                                                                       '_');
        $this->assertTrue($result['errCode'] == 400060);
    }

    public function test0290ContainsSpecialCharactersTic()
    {
        $myContentHelper = $this->getContentHelperInstance();
        $result          = $myContentHelper->containsSpecialCharacters('Scott',
                                                                       '`');
        $this->assertTrue($result['errCode'] == 400060);
    }

    public function test0300ContainsSpecialCharactersLeftCurly()
    {
        $myContentHelper = $this->getContentHelperInstance();
        $result          = $myContentHelper->containsSpecialCharacters('Scott',
                                                                       '{');
        $this->assertTrue($result['errCode'] == 400060);
    }

    public function test0310ContainsSpecialCharactersVerticalBar()
    {
        $myContentHelper = $this->getContentHelperInstance();
        $result          = $myContentHelper->containsSpecialCharacters('Scott',
                                                                       '|');
        $this->assertTrue($result['errCode'] == 400060);
    }

    public function test0320ContainsSpecialCharactersRightCurly()
    {
        $myContentHelper = $this->getContentHelperInstance();
        $result          = $myContentHelper->containsSpecialCharacters('Scott',
                                                                       '}');
        $this->assertTrue($result['errCode'] == 400060);
    }

    public function test0330ContainsSpecialCharactersTilda()
    {
        $myContentHelper = $this->getContentHelperInstance();
        $result          = $myContentHelper->containsSpecialCharacters('Scott',
                                                                       '~');
        $this->assertTrue($result['errCode'] == 400060);
    }

    public function test0340ValidateCountryCode002()
    {
        $myContentHelper = $this->getContentHelperInstance();
        $result          = $myContentHelper->validateCountryCode('Scott',
                                                                 002);
        $this->assertTrue($result['errCode'] == 200250);
    }

    public function test0350ValidateCountryCode254()
    {
        $myContentHelper = $this->getContentHelperInstance();
        $result          = $myContentHelper->validateCountryCode('Scott',
                                                                 254);
        $this->assertTrue($result['errCode'] == 0);
    }

    public function test0360ValidateEmailFormat()
    {
        $myContentHelper = $this->getContentHelperInstance();
        $result          = $myContentHelper->validateEmailFormat('Scott',
                                                                 'test@getsote.com');
        $this->assertTrue($result['errCode'] == 0);
    }

    public function test0370ValidateBadEmailFormat1()
    {
        $myContentHelper = $this->getContentHelperInstance();
        $result          = $myContentHelper->validateEmailFormat('Scott',
                                                                 '@getsote.com');
        $this->assertTrue($result['errCode'] == 400050);
    }

    public function test0380ValidateBadEmailFormat2()
    {
        $myContentHelper = $this->getContentHelperInstance();
        $result          = $myContentHelper->validateEmailFormat('Scott',
                                                                 'Test@');
        $this->assertTrue($result['errCode'] == 400050);
    }

    public function test0390ValidateBadEmailFormat3()
    {
        $myContentHelper = $this->getContentHelperInstance();
        $result          = $myContentHelper->validateEmailFormat('Scott',
                                                                 'Test@getsote');
        $this->assertTrue($result['errCode'] == 400050);
    }

    public function test0400ValidateBadEmailFormat4()
    {
        $myContentHelper = $this->getContentHelperInstance();
        $result          = $myContentHelper->validateEmailFormat('Scott',
                                                                 'Test');
        $this->assertTrue($result['errCode'] == 400050);
    }

    public function test0410ValidateBadEmailFormat5()
    {
        $myContentHelper = $this->getContentHelperInstance();
        $result          = $myContentHelper->validateEmailFormat('Scott',
                                                                 '.getsote');
        $this->assertTrue($result['errCode'] == 400050);
    }

    public function test0420ValidateDatewithslash()
    {
        $myContentHelper = $this->getContentHelperInstance();
        $result          = $myContentHelper->validateDate('Scott',
                                                          '1200/01/01');
        $this->assertTrue($result['errCode'] == 0);
    }

    public function test0430ValidateDatewithDash()
    {
        $myContentHelper = $this->getContentHelperInstance();
        $result          = $myContentHelper->validateDate('Scott',
                                                          '1200-01-01');
        $this->assertTrue($result['errCode'] == 0);
    }

    public function test0440ValidateDatewithPeriod()
    {
        $myContentHelper = $this->getContentHelperInstance();
        $result          = $myContentHelper->validateDate('Scott',
                                                          '1200.01.01');
        $this->assertTrue($result['errCode'] == 0);
    }

    public function test0450ValidateDatewithDollarSign()
    {
        $myContentHelper = $this->getContentHelperInstance();
        $result          = $myContentHelper->validateDate('Scott',
                                                          '1200$01$01');
        $this->assertTrue($result['errCode'] == 200250);
    }

    public function test0460ValidateDateNegativeYear()
    {
        $myContentHelper = $this->getContentHelperInstance();
        $result          = $myContentHelper->validateDate('Scott',
                                                          '-1200/01/01');
        $this->assertTrue($result['errCode'] == 400070);
    }

    public function test0470ValidateDateNonNumericYear()
    {
        $myContentHelper = $this->getContentHelperInstance();
        $result          = $myContentHelper->validateDate('Scott',
                                                          '12X0/01/01');
        $this->assertTrue($result['errCode'] == 400000);
    }

    public function test0480ValidateDateNonNumericMonth()
    {
        $myContentHelper = $this->getContentHelperInstance();
        $result          = $myContentHelper->validateDate('Scott',
                                                          '12X0/01/01');
        $this->assertTrue($result['errCode'] == 400000);
    }

    public function test0490ValidateDateNonNumericDay()
    {
        $myContentHelper = $this->getContentHelperInstance();
        $result          = $myContentHelper->validateDate('Scott',
                                                          '12X0/01/01');
        $this->assertTrue($result['errCode'] == 400000);
    }

    public function test0500ValidateDateBadMonth()
    {
        $myContentHelper = $this->getContentHelperInstance();
        $result          = $myContentHelper->validateDate('Scott',
                                                          '1200/00/01');
        $this->assertTrue($result['errCode'] == 400070);
    }

    public function test0510ValidateDateBadDay()
    {
        $myContentHelper = $this->getContentHelperInstance();
        $result          = $myContentHelper->validateDate('Scott',
                                                          '1200/01/00');
        $this->assertTrue($result['errCode'] == 400070);
    }

    public function test0520ValidateTimestampZulu()
    {
        $myContentHelper = $this->getContentHelperInstance();
        $result          = $myContentHelper->validateTimestamp('Scott',
                                                               '2018-10-07T20:00:14.080Z');
        $this->assertTrue($result['errCode'] == 0);
    }

    public function test0530ValidateTimestampGMT()
    {
        $myContentHelper = $this->getContentHelperInstance();
        $result          = $myContentHelper->validateTimestamp('Scott',
                                                               'Sun, 07 Oct 2018 19:46:06 GMT');
        $this->assertTrue($result['errCode'] == 0);
    }

    public function test0540ValidateTimestampInvalidFormat()
    {
        $myContentHelper = $this->getContentHelperInstance();
        $result          = $myContentHelper->validateTimestamp('Scott',
                                                               'Sun, 07 QK2 2018 19:46:06 GMT');
        $this->assertTrue($result['errCode'] == 400080);
    }

    public function test0550ListCountryCodes()
    {
        $myContentHelper = $this->getContentHelperInstance();
        $result          = $myContentHelper->listCountryCodes();
        $this->assertArrayHasKey('USA', $result['retPack']['country_codes']);
    }

    public function test0560ValidateCountryCode()
    {
        $myContentHelper = $this->getContentHelperInstance();
        $result          = $myContentHelper->validateCountryCode('country_code',
                                                               '001');
        $this->assertTrue($result['errCode'] == 0);
    }

    public function test0570ValidateCountryCodeNotFound()
    {
        $myContentHelper = $this->getContentHelperInstance();
        $result          = $myContentHelper->validateCountryCode('country_code',
                                                               '');
        $this->assertTrue($result['errCode'] == 200250);
    }

    public function test0580ListPayPeriods()
    {
        $myContentHelper = $this->getContentHelperInstance();
        $result          = $myContentHelper->listPayPeriods();
        $this->assertArrayHasKey('lower_case', $result['retPack']);
    }

    public function test0580ValidatePayPeriod()
    {
        $myContentHelper = $this->getContentHelperInstance();
        $result          = $myContentHelper->validatePayPeriod('wage',
                                                               'TWO_WEEKS');
        $this->assertTrue($result['errCode'] == 0);
    }

    public function test0590ValidatePayPeriodNotFound()
    {
        $myContentHelper = $this->getContentHelperInstance();
        $result          = $myContentHelper->validatePayPeriod('wage',
                                                               '');
        $this->assertTrue($result['errCode'] == 200250);
    }
    public function test0600ValidateTrueFalseMixedCase()
    {
        $myContentHelper = $this->getContentHelperInstance();
        $result          = $myContentHelper->validateTrueFalse('Mary',
                                                                       'True');
        $this->assertTrue($result['errCode'] == 0);
    }
    public function test0610ValidateTrueFalseLowerCase()
    {
        $myContentHelper = $this->getContentHelperInstance();
        $result          = $myContentHelper->validateTrueFalse('Mary',
                                                                       'true');
        $this->assertTrue($result['errCode'] == 0);
    }
    public function test0620ValidateTrueFalseUpcaseCase()
    {
        $myContentHelper = $this->getContentHelperInstance();
        $result          = $myContentHelper->validateTrueFalse('Mary',
                                                                       'FALSE');
        $this->assertTrue($result['errCode'] == 0);
    }
    public function test0630ValidateTrueFalseNotFound()
    {
        $myContentHelper = $this->getContentHelperInstance();
        $result          = $myContentHelper->validateTrueFalse('Mary',
                                                                       'MaryValue');
        $this->assertTrue($result['errCode'] == 200250);
    }
    public function test0640validatePhoneNumber()
    {
        $myContentHelper = $this->getContentHelperInstance();
        $result          = $myContentHelper->validatePhoneNumber('12345678');
        $this->assertTrue($result['errCode'] == 0);
    } 
    public function test0650validatePhoneNumber6Digits()
    {
        $myContentHelper = $this->getContentHelperInstance();
        $result          = $myContentHelper->validatePhoneNumber('123456');
        $this->assertTrue($result['errCode'] == 400090);
    }
    public function test0660validatePhoneNumber11Digits()
    {
        $myContentHelper = $this->getContentHelperInstance();
        $result          = $myContentHelper->validatePhoneNumber('12345678901');
        $this->assertTrue($result['errCode'] == 400090);
    }
    public function test0670validatePhoneNumberNotNumeric()
    {
        $myContentHelper = $this->getContentHelperInstance();
        $result          = $myContentHelper->validatePhoneNumber('Mary');
        $this->assertTrue($result['errCode'] == 400000);
    }
    public function test0680containsAlphaUnderscoreChars()
    {
        $myContentHelper = $this->getContentHelperInstance();
        $result          = $myContentHelper->containsAlphaUnderscoreChars('Mary',
                                                                       'Mary_Value');
        $this->assertTrue($result['errCode'] == 0);
    }
    public function test0690containsAlphaUnderscoreCharsAlphas()
    {
        $myContentHelper = $this->getContentHelperInstance();
        $result          = $myContentHelper->containsAlphaUnderscoreChars('Mary',
                                                                       'MaryValue');
        $this->assertTrue($result['errCode'] == 0);
    }
    public function test0700containsAlphaUnderscoreCharsUnderscores()
    {
        $myContentHelper = $this->getContentHelperInstance();
        $result          = $myContentHelper->containsAlphaUnderscoreChars('Mary',
                                                                       '____');
        $this->assertTrue($result['errCode'] == 0);
    }
    public function test0710containsAlphaUnderscoreCharsNumerics()
    {
        $myContentHelper = $this->getContentHelperInstance();
        $result          = $myContentHelper->containsAlphaUnderscoreChars('Mary',
                                                                       '1MaryValue');
        $this->assertTrue($result['errCode'] == 400065);
    } 
    public function test0720containsAlphaUnderscoreCharsSpace()
    {
        $myContentHelper = $this->getContentHelperInstance();
        $result          = $myContentHelper->containsAlphaUnderscoreChars('Mary',
                                                                       'Mary Value');
        $this->assertTrue($result['errCode'] == 400065);
    }
    public function test0730containsAlphaUnderscoreCharsOtherSpecialChars()
    {
        $myContentHelper = $this->getContentHelperInstance();
        $result          = $myContentHelper->containsAlphaUnderscoreChars('Mary',
                                                                       'Mary#$%Value');
        $this->assertTrue($result['errCode'] == 400065);
    }    
    public function test0740formatCountryCodeWithSign()
    {
        $myContentHelper = $this->getContentHelperInstance();
        $result          = $myContentHelper->formatCountryCode('Mary',
                                                                       '+1234567');
        $this->assertTrue($result['errCode'] == 0);
        $this->assertTrue(substr($result['retPack']['Mary'], 0) == '+1234567');
    }
    public function test0750formatCountryCodeNoSign()
    {
        $myContentHelper = $this->getContentHelperInstance();
        $result          = $myContentHelper->formatCountryCode('Mary',
                                                                       '1234567');
        $this->assertTrue($result['errCode'] == 0);
        $this->assertTrue(substr($result['retPack']['Mary'], 0) == '+1234567');

    }

    public function test0760validatePaymentMethodsLowerCase()
    {
        $myContentHelper = $this->getContentHelperInstance();
        $result          = $myContentHelper->validatePaymentMethods('Mary',
                                                                       'mpesa');
        $this->assertTrue($result['errCode'] == 0);
    }    
    public function test0770validatePaymentMethodsUpperCase()
    {
        $myContentHelper = $this->getContentHelperInstance();
        $result          = $myContentHelper->validatePaymentMethods('Mary',
                                                                       'CREDIT');
        $this->assertTrue($result['errCode'] == 0);
    }
    public function test0780validatePaymentMethodsMixedCase()
    {
        $myContentHelper = $this->getContentHelperInstance();
        $result          = $myContentHelper->validatePaymentMethods('Mary',
                                                                       'Mpesa');
        $this->assertTrue($result['errCode'] == 0);
    }
    public function test0790validatePaymentMethodsNotFound()
    {
        $myContentHelper = $this->getContentHelperInstance();
        $result          = $myContentHelper->validatePaymentMethods('Mary',
                                                                       'MaryValue');
        $this->assertTrue($result['errCode'] == 200250);
        $this->assertTrue(strpos($result['statusText'], 'Mary') == 0);
        $this->assertTrue(strpos($result['statusText'], 'MaryValue') > 0);
        $this->assertTrue(strpos($result['statusText'], 'Mpesa') > 0);
    }
    public function test0800listPaymentMethods()
    {
        $myContentHelper = $this->getContentHelperInstance();
        $result          = $myContentHelper->listPaymentMethods();
        $this->assertArrayHasKey('mixed_case', $result['retPack']);
    }
        public function test00810ContainsInvalidChars()
    {
        $myContentHelper = $this->getContentHelperInstance();
        $result          = $myContentHelper->containsInvalidChars('Mary',
                                                                       'MaryValue');
        $this->assertTrue($result['errCode'] == 0);
    }

    public function test00820ContainsInvalidCharsExclamation()
    {
        $myContentHelper = $this->getContentHelperInstance();
        $result          = $myContentHelper->containsInvalidChars('Mary',
                                                                       '!');
        $this->assertTrue($result['errCode'] == 0);
    }

    public function test00830ContainsInvalidCharsDoubleQoute()
    {
        $myContentHelper = $this->getContentHelperInstance();
        $result          = $myContentHelper->containsInvalidChars('Mary',
                                                                       '"');
        $this->assertTrue($result['errCode'] == 400060);
    }

    public function test00840ContainsInvalidCharsHash()
    {
        $myContentHelper = $this->getContentHelperInstance();
        $result          = $myContentHelper->containsInvalidChars('Mary',
                                                                       '#');
        $this->assertTrue($result['errCode'] == 0);
    }

    public function test00850ContainsInvalidCharsDollarSign()
    {
        $myContentHelper = $this->getContentHelperInstance();
        $result          = $myContentHelper->containsInvalidChars('Mary',
                                                                       '$');
        $this->assertTrue($result['errCode'] == 0);
    }

    public function test00860ContainsInvalidCharsPercent()
    {
        $myContentHelper = $this->getContentHelperInstance();
        $result          = $myContentHelper->containsInvalidChars('Mary',
                                                                       '%');
        $this->assertTrue($result['errCode'] == 400060);
    }

    public function test00870ContainsInvalidCharsAmpersand()
    {
        $myContentHelper = $this->getContentHelperInstance();
        $result          = $myContentHelper->containsInvalidChars('Mary',
                                                                       '&');
        $this->assertTrue($result['errCode'] == 400060);
    }

    public function test0890ContainsInvalidCharsSingleQoute()
    {
        $myContentHelper = $this->getContentHelperInstance();
        $result          = $myContentHelper->containsInvalidChars('Mary',
                                                                       "'");
        $this->assertTrue($result['errCode'] == 400060);
    }

    public function test0900ContainsInvalidCharsLeftParen()
    {
        $myContentHelper = $this->getContentHelperInstance();
        $result          = $myContentHelper->containsInvalidChars('Mary',
                                                                       '(');
        $this->assertTrue($result['errCode'] == 400060);
    }

    public function test0910ContainsInvalidCharsRightParen()
    {
        $myContentHelper = $this->getContentHelperInstance();
        $result          = $myContentHelper->containsInvalidChars('Mary',
                                                                       ')');
        $this->assertTrue($result['errCode'] == 400060);
    }

    public function test0920ContainsInvalidCharsAsterick()
    {
        $myContentHelper = $this->getContentHelperInstance();
        $result          = $myContentHelper->containsInvalidChars('Mary',
                                                                       '*');
        $this->assertTrue($result['errCode'] == 400060);
    }

    public function test0930ContainsInvalidCharsPlus()
    {
        $myContentHelper = $this->getContentHelperInstance();
        $result          = $myContentHelper->containsInvalidChars('Mary',
                                                                       '+');
        $this->assertTrue($result['errCode'] == 0);
    }

    public function test0940ContainsInvalidCharsComma()
    {
        $myContentHelper = $this->getContentHelperInstance();
        $result          = $myContentHelper->containsInvalidChars('Mary',
                                                                       ',');
        $this->assertTrue($result['errCode'] == 0);
    }

    public function test0950ContainsInvalidCharsMinus()
    {
        $myContentHelper = $this->getContentHelperInstance();
        $result          = $myContentHelper->containsInvalidChars('Mary',
                                                                       '-');
        $this->assertTrue($result['errCode'] == 0);
    }

    public function test0960ContainsInvalidCharsPeriod()
    {
        $myContentHelper = $this->getContentHelperInstance();
        $result          = $myContentHelper->containsInvalidChars('Mary',
                                                                       '.');
        $this->assertTrue($result['errCode'] == 0);
    }

    public function test0970ContainsInvalidCharsColon()
    {
        $myContentHelper = $this->getContentHelperInstance();
        $result          = $myContentHelper->containsInvalidChars('Mary',
                                                                       ':');
        $this->assertTrue($result['errCode'] == 0);
    }

    public function test0980ContainsInvalidCharsSemiColon()
    {
        $myContentHelper = $this->getContentHelperInstance();
        $result          = $myContentHelper->containsInvalidChars('Mary',
                                                                       ';');
        $this->assertTrue($result['errCode'] == 0);
    }

    public function test0990ContainsInvalidCharsLessThan()
    {
        $myContentHelper = $this->getContentHelperInstance();
        $result          = $myContentHelper->containsInvalidChars('Mary',
                                                                       '<');
        $this->assertTrue($result['errCode'] == 0);
    }

    public function test01000ContainsInvalidCharsEqual()
    {
        $myContentHelper = $this->getContentHelperInstance();
        $result          = $myContentHelper->containsInvalidChars('Mary',
                                                                       '=');
        $this->assertTrue($result['errCode'] == 0);
    }

    public function test01010ContainsInvalidCharsGreaterThan()
    {
        $myContentHelper = $this->getContentHelperInstance();
        $result          = $myContentHelper->containsInvalidChars('Mary',
                                                                       '>');
        $this->assertTrue($result['errCode'] == 0);
    }

    public function test01020ContainsInvalidCharsQuestion()
    {
        $myContentHelper = $this->getContentHelperInstance();
        $result          = $myContentHelper->containsInvalidChars('Mary',
                                                                       '?');
        $this->assertTrue($result['errCode'] == 0);
    }

    public function test01030ContainsInvalidCharsAtsign()
    {
        $myContentHelper = $this->getContentHelperInstance();
        $result          = $myContentHelper->containsInvalidChars('Mary',
                                                                       '@');
        $this->assertTrue($result['errCode'] == 0);
    }

    public function test01040ContainsInvalidCharsLeftBracket()
    {
        $myContentHelper = $this->getContentHelperInstance();
        $result          = $myContentHelper->containsInvalidChars('Mary',
                                                                       '[');
        $this->assertTrue($result['errCode'] == 400060);
    }

    public function test01050ContainsInvalidCharsBackSlash()
    {
        $myContentHelper = $this->getContentHelperInstance();
        $result          = $myContentHelper->containsInvalidChars('Mary',
                                                                       '\\');
        $this->assertTrue($result['errCode'] == 400060);
    }

    public function test01060ContainsInvalidCharsRightBracket()
    {
        $myContentHelper = $this->getContentHelperInstance();
        $result          = $myContentHelper->containsInvalidChars('Mary',
                                                                       ']');
        $this->assertTrue($result['errCode'] == 400060);
    }

    public function test01070ContainsInvalidCharsCaret()
    {
        $myContentHelper = $this->getContentHelperInstance();
        $result          = $myContentHelper->containsInvalidChars('Mary',
                                                                       '^');
        $this->assertTrue($result['errCode'] == 400060);
    }

    public function test01080ContainsInvalidCharsUnderscore()
    {
        $myContentHelper = $this->getContentHelperInstance();
        $result          = $myContentHelper->containsInvalidChars('Mary',
                                                                       '_');
        $this->assertTrue($result['errCode'] == 400060);
    }

    public function test01090ContainsInvalidCharsTic()
    {
        $myContentHelper = $this->getContentHelperInstance();
        $result          = $myContentHelper->containsInvalidChars('Mary',
                                                                       '`');
        $this->assertTrue($result['errCode'] == 400060);
    }

    public function test01100ContainsInvalidCharsLeftCurly()
    {
        $myContentHelper = $this->getContentHelperInstance();
        $result          = $myContentHelper->containsInvalidChars('Mary',
                                                                       '{');
        $this->assertTrue($result['errCode'] == 400060);
    }

    public function test01110ContainsInvalidCharsVerticalBar()
    {
        $myContentHelper = $this->getContentHelperInstance();
        $result          = $myContentHelper->containsInvalidChars('Mary',
                                                                       '|');
        $this->assertTrue($result['errCode'] == 400060);
    }

    public function test01120ContainsInvalidCharsRightCurly()
    {
        $myContentHelper = $this->getContentHelperInstance();
        $result          = $myContentHelper->containsInvalidChars('Mary',
                                                                       '}');
        $this->assertTrue($result['errCode'] == 400060);
    }

    public function test01130ContainsInvalidCharsTilda()
    {
        $myContentHelper = $this->getContentHelperInstance();
        $result          = $myContentHelper->containsInvalidChars('Mary',
                                                                       '~');
        $this->assertTrue($result['errCode'] == 400060);
    }
    public function test01140containsAlphaSpaceChars()
    {
        $myContentHelper = $this->getContentHelperInstance();
        $result          = $myContentHelper->containsAlphaSpaceChars('Mary',
                                                                       'Mary Value');
        $this->assertTrue($result['errCode'] == 0);
    }
    public function test01150containsAlphaSpaceCharsAlphas()
    {
        $myContentHelper = $this->getContentHelperInstance();
        $result          = $myContentHelper->containsAlphaSpaceChars('Mary',
                                                                       'MaryValue');
        $this->assertTrue($result['errCode'] == 0);
    }
    public function test01160containsAlphaSpaceCharsSpaces()
    {
        $myContentHelper = $this->getContentHelperInstance();
        $result          = $myContentHelper->containsAlphaSpaceChars('Mary',
                                                                       '    ');
        $this->assertTrue($result['errCode'] == 0);
    }
    public function test01170containsAlphaSpaceCharsNumerics()
    {
        $myContentHelper = $this->getContentHelperInstance();
        $result          = $myContentHelper->containsAlphaSpaceChars('Mary',
                                                                       '1MaryValue');
        $this->assertTrue($result['errCode'] == 400065);
    }
    public function test01180containsAlphaSpaceCharsOtherSpecialChars()
    {
        $myContentHelper = $this->getContentHelperInstance();
        $result          = $myContentHelper->containsAlphaSpaceChars('Mary',
                                                                       'Mary#$%Value');
        $this->assertTrue($result['errCode'] == 400065);
    }  
    public function test01190containsAlphaSpaceCharsUnderscores()
    {
        $myContentHelper = $this->getContentHelperInstance();
        $result          = $myContentHelper->containsAlphaSpaceChars('Mary',
                                                                       '____');
        $this->assertTrue($result['errCode'] == 400065);
    }
    public function test01200ValidateCurrencyTypeGBP()
    {
        $myContentHelper = $this->getContentHelperInstance();
        $result          = $myContentHelper->validateCurrencyType('Mary',
                                                                  'GBP');
        $this->assertTrue($result['errCode'] == 0);
    }

    public function test01210ValidateCurrencyTypeNAN()
    {
        $myContentHelper = $this->getContentHelperInstance();
        $result          = $myContentHelper->validateCurrencyType('Mary',
                                                                  'NAN');
        $this->assertTrue($result['errCode'] == 200250);
    }
    public function test01220ValidateCurrencyType()
    {
        $myContentHelper = $this->getContentHelperInstance();
        $result          = $myContentHelper->validateCurrencyType('currency_type',
                                                               'KES');
        $this->assertTrue($result['errCode'] == 0);
    }

    public function test01230ValidateCurrencyTypeNotFound()
    {
        $myContentHelper = $this->getContentHelperInstance();
        $result          = $myContentHelper->validateCurrencyType('currency_type',
                                                               '');
        $this->assertTrue($result['errCode'] == 200250);
    }
    public function test01240ListCurrencyTypes()
    {
        $myContentHelper = $this->getContentHelperInstance();
        $result          = $myContentHelper->listCurrencyTypes();
        $this->assertArrayHasKey('Kenya', $result['retPack']['currency_types']);
    } 
    public function test01250containsAlphaSpaceChars()
    {
        $myContentHelper = $this->getContentHelperInstance();
        $result          = $myContentHelper->containsAlphaSpaceCommaChars('Mary',
                                                                          'Mary Value');
        $this->assertTrue($result['errCode'] == 0);
    }
    public function test01255containsAlphaSpaceCharsAlphas()
    {
        $myContentHelper = $this->getContentHelperInstance();
        $result          = $myContentHelper->containsAlphaSpaceCommaChars('Mary',
                                                                          'MaryValue');
        $this->assertTrue($result['errCode'] == 0);
    }
    public function test01260containsAlphaSpaceCharsSpaces()
    {
        $myContentHelper = $this->getContentHelperInstance();
        $result          = $myContentHelper->containsAlphaSpaceCommaChars('Mary',
                                                                          '    ');
        $this->assertTrue($result['errCode'] == 0);
    }
    public function test01265containsAlphaSpaceCharsCommas()
    {
        $myContentHelper = $this->getContentHelperInstance();
        $result          = $myContentHelper->containsAlphaSpaceCommaChars('Mary',
                                                                          'Mary,Makau');
        $this->assertTrue($result['errCode'] == 0);
    }
    public function test01270containsAlphaSpaceCharsNumerics()
    {
        $myContentHelper = $this->getContentHelperInstance();
        $result          = $myContentHelper->containsAlphaSpaceCommaChars('Mary',
                                                                          '1MaryValue');
        $this->assertTrue($result['errCode'] == 400065);
    }
    public function test01280containsAlphaSpaceCharsOtherSpecialChars()
    {
        $myContentHelper = $this->getContentHelperInstance();
        $result          = $myContentHelper->containsAlphaSpaceCommaChars('Mary',
                                                                          'Mary#$%Value');
        $this->assertTrue($result['errCode'] == 400065);
    }  
    public function test01290containsAlphaSpaceCharsUnderscores()
    {
        $myContentHelper = $this->getContentHelperInstance();
        $result          = $myContentHelper->containsAlphaSpaceCommaChars('Mary',
                                                                          '____');
        $this->assertTrue($result['errCode'] == 400065);
    }      
    protected function getContentHelperInstance()
    {
        return new ContentHelper($this->objLogger);
    }

}