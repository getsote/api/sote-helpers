<?php

namespace Tests;

use HELPERS\DBHelper;
use Doctrine\DBAL\DriverManager;

class DateHelperTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;
    /**
     * @var object $db The instance of the Doctrine DBAL
     */
    protected $objDB;
    /**
     * @var "Slim\Http\RequestMonolog\Logger" $logger The instance of the Logger created at startup.
     */
    protected $objLogger;

    protected function _before()
    {
        // Start Logger
        $this->objLogger = new \Monolog\Logger('sote-helper');
        $this->objLogger->pushProcessor(new \Monolog\Processor\UidProcessor());
        $this->objLogger->pushHandler(new \Monolog\Handler\StreamHandler('php://stdout',
                                                                         'DEBUG'));

        // Connect to Database
        $connParms       = array('driver'   => 'pdo_pgsql',
                                 'host'     => 'postgres',
                                 'port'     => '5432',
                                 'dbname'   => 'sote_development',
                                 'user'     => 'sote',
                                 'password' => 'password');
        $this->objDB = DriverManager::getConnection($connParms);
    }

    protected function _after()
    {
    }

    // tests
    public function test0010DBHelperInstance()
    {
        $myDBHelper = $this->createDBHelperInstance();
        $this->assertTrue(is_object($myDBHelper));
    }

    public function test0020ValidateRowVersionValidTable()
    {
        $myDBHelper = $this->createDBHelperInstance();
        $result     = $myDBHelper->validateRowVersion('organizations',
                                                      1000,
                                                      1);
        $this->assertTrue($result['errCode'] == 0);
    }

    public function test0030ValidateRowVersionBadRowVersion()
    {
        $myDBHelper = $this->createDBHelperInstance();
        $result     = $myDBHelper->validateRowVersion('organizations',
                                                      1000,
                                                      2);
        $this->assertTrue($result['errCode'] == 200000);
    }

    public function test0040ValidateRowVersionInvalidTable()
    {
        $myDBHelper = $this->createDBHelperInstance();
        $result     = $myDBHelper->validateRowVersion('organizat',
                                                      1000,
                                                      1);
        $this->assertTrue($result['errCode'] == 200100);
    }
    public function test0050FormatSQLArrayString()
    {
        $myDBHelper = $this->createDBHelperInstance();
        $result     = $myDBHelper->formatSQLArrayString(json_encode(["Mary","Makau"]));
        $this->assertTrue($result['errCode'] == 0);
    }
    protected function createDBHelperInstance()
    {
        return new DBHelper($this->objLogger,
                            $this->objDB);
    }
}