<?php
/**
 * Helper class for content
 *
 * These methods are used any where in the system that needed to validate, modify, etc to field values.
 *
 */

/**
 * Created by PhpStorm.
 * User: syacko
 * Date: 7/16/18
 * Time: 9:10 AM
 * Author:    Scott
 * Co-author: Mary
 */

namespace HELPERS;

/**
 * Class ContentHelper
 */
class ContentHelper
{
    /**
     * @var object $logger The instance of the "Slim\Http\RequestMonolog\Logger" created at startup.
     */
    protected $objLogger;

    /**
     * Class Variable area
     */
    protected $myCountryCodes;
    protected $myCurrenctTypes;
    protected $displayPayPeriods = 'Weekly, weekly, WEEKLY, Two_Weeks, two_weeks, TWO_WEEKS, Half_Monthly, half_monthly, HALF_MONTHLY, Monthly, monthly, MONTHLY, Yearly, yearly, YEARLY';
    protected $displayPaymentMethods = 'Mpesa, mpesa, MPESA, Credit, credit, CREDIT';

    /**
     * contains Special Characters will test if the following characters are in the string
     * !"#\$%&''()*+,-./:;<=>?@[\]^_`{|}~
     *
     * @param string $myFieldName
     * @param string $myFieldValue
     *
     * @return array Keys: errCode, statusText, codeLoc, custMsg, retPack
     *               retPack on an error will return special_characters (key) with the list of special characters (value)
     */
    public function containsSpecialCharacters(string $myFieldName,
                                              string $myFieldValue): array
    {
        $this->objLogger->debug(__METHOD__);

        if (preg_match_all('/[!"#\$%&\'\'\(\)\*\+,-\.\/:;<=>\?@\[\\\\\]\^_`\{\|}~]/m',
                           $myFieldValue))
        {
            $result = array('errCode'    => 400060,
                            'statusText' => $myFieldName . ' (' . $myFieldValue . ') contains special characters which are not allowed',
                            'codeLoc'    => __METHOD__,
                            'custMsg'    => '',
                            'retPack'    => array('special_characters' => '!"#\$%&\'\'()*+,-./:;<=>?@[\]^_`{|}~'));
        } else
        {
            $result = array('errCode'    => 0,
                            'statusText' => 'Success',
                            'codeLoc'    => __METHOD__,
                            'custMsg'    => '',
                            'retPack'    => '');
        }
        $result['codeLoc'] = __METHOD__;
        $this->objLogger->debug('errCode: ' . $result['errCode'] . ' statusText: ' . $result['statusText'] . ' codeLoc: ' . __METHOD__ . ' custMsg: NA retPack: NA');

        return $result;
    }

    /**
     * note contains invalid Characters tests string and returns error if true
     * "\$%&''()*\/[\]^_`{|}~
     *
     * @param string $myFieldName
     * @param string $myFieldValue
     *
     * @return array Keys: errCode, statusText, codeLoc, custMsg, retPack
     *               retPack on an error will return invalid_characters (key) with the list of special characters (value)
     */
    public function containsInvalidChars(string $myFieldName,
                                              string $myFieldValue): array
    {
        $this->objLogger->debug(__METHOD__);

        if (preg_match_all('/["\%&\'\'\(\)\*\/\[\\\\\]\^_`\{\|}~]/m',
                           $myFieldValue))
        {
            $result = array('errCode'    => 400060,
                            'statusText' => $myFieldName . ' (' . $myFieldValue . ') contains invalid characters which are not allowed',
                            'codeLoc'    => __METHOD__,
                            'custMsg'    => '',
                            'retPack'    => array('invalid_characters' => '"\$%&\'()*\/[\]^_`{|}~'));
        } else
        {
            $result = array('errCode'    => 0,
                            'statusText' => 'Success',
                            'codeLoc'    => __METHOD__,
                            'custMsg'    => '',
                            'retPack'    => '');
        }
        $result['codeLoc'] = __METHOD__;
        $this->objLogger->debug('errCode: ' . $result['errCode'] . ' statusText: ' . $result['statusText'] . ' codeLoc: ' . __METHOD__ . ' custMsg: NA retPack: NA');

        return $result;
    }

    /**
     * list Country Codes will list out available codes
     *
     * @return array Keys: errCode, statusText, codeLoc, custMsg, retPack
     */
    public function listCountryCodes(): array
    {
        $this->objLogger->debug(__METHOD__);

        $result = array('errCode'    => 0,
                        'statusText' => 'Success',
                        'codeLoc'    => __METHOD__,
                        'custMsg'    => '',
                        'retPack'    => array('country_codes' => $this->myCountryCodes));
        $this->objLogger->debug('errCode: ' . $result['errCode'] . ' statusText: ' . $result['statusText'] . ' codeLoc: ' . __METHOD__ . ' custMsg: NA retPack: NA');

        return $result;
    }

    /**
     * list currency types will list out available codes
     *
     * @return array Keys: errCode, statusText, codeLoc, custMsg, retPack
     */
    public function listCurrencyTypes(): array
    {
        $this->objLogger->debug(__METHOD__);

        $result = array('errCode'    => 0,
                        'statusText' => 'Success',
                        'codeLoc'    => __METHOD__,
                        'custMsg'    => '',
                        'retPack'    => array('currency_types' => $this->myCurrenctTypes));
        $this->objLogger->debug('errCode: ' . $result['errCode'] . ' statusText: ' . $result['statusText'] . ' codeLoc: ' . __METHOD__ . ' custMsg: NA retPack: NA');

        return $result;
    }


    /**
     * This will provide a list of valid Pay Periods
     *
     * NOTE: The frequency that these values change is extremely limited, so it is hard coded.
     * NOTE: If the frequency of change increase, this should be migrated to a database table.
     *
     *
     * @return array Keys: errCode, statusText, codeLoc, custMsg, retPack
     */
    public function listPayPeriods(): array
    {
        $this->objLogger->debug(__METHOD__);

        $this->objLogger->debug('errCode: 0 statusText: Success codeLoc: ' . __METHOD__ . ' custMsg: NA retPack: NA');
        $result = array('errCode'    => 0,
                        'statusText' => 'Success',
                        'codeLoc'    => __METHOD__,
                        'custMsg'    => '',
                        'retPack'    => array('lower_case' => array('pgm_code'      => array('weekly',
                                                                                             'two_weeks',
                                                                                             'half_monthly',
                                                                                             'monthly',
                                                                                             'yearly'),
                                                                    'display_value' => array('weekly',
                                                                                             'two weeks',
                                                                                             'half monthly',
                                                                                             'monthly',
                                                                                             'yearly')),
                                              'mixed_case' => array('pgm_code'      => array('Weekly',
                                                                                             'Two_Weeks',
                                                                                             'Half_Monthly',
                                                                                             'Monthly',
                                                                                             'Yearly'),
                                                                    'display_value' => array('Weekly',
                                                                                             'Two Weeks',
                                                                                             'Half Monthly',
                                                                                             'Monthly',
                                                                                             'Yearly')),
                                              'upper_case' => array('pgm_code'      => array('WEEKLY',
                                                                                             'TWO_WEEKS',
                                                                                             'HALF_MONTHLY',
                                                                                             'MONTHLY',
                                                                                             'YEARLY'),
                                                                    'display_value' => array('WEEKLY',
                                                                                             'TWO WEEKS',
                                                                                             'HALF MONTHLY',
                                                                                             'MONTHLY',
                                                                                             'YEARLY'))));
        $this->objLogger->debug('errCode: ' . $result['errCode'] . ' statusText: ' . $result['statusText'] . ' codeLoc: ' . __METHOD__ . ' custMsg: NA retPack: NA');

        return $result;
    }

    /**
     * This will provide a list of valid Pay Periods
     *
     * NOTE: The frequency that these values change is extremely limited, so it is hard coded.
     * NOTE: If the frequency of change increase, this should be migrated to a database table.
     *
     *
     * @param string $myFieldName
     * @param string $myFieldValue
     *
     * @return array Keys: errCode, statusText, codeLoc, custMsg, retPack
     *               retPack on an error will return valid_country_codes (key) with the list of country codes (value)
     */
    public function validateCountryCode(string $myFieldName,
                                        string $myFieldValue): array
    {
        $this->objLogger->debug(__METHOD__);

        if (substr($myFieldValue,
                   0,
                   1) == '+')
        {
            $myFieldValue = substr($myFieldValue,
                                   1,
                                   strlen($myFieldValue));
        }

        $searchResult = array_search($myFieldValue,
                                     $this->myCountryCodes);

        if ($searchResult === FALSE)
        {
            $result = array('errCode'    => 200250,
                            'statusText' => $myFieldName . ' (' . $myFieldValue . ') must contain one of these values: ' . json_encode($this->myCountryCodes),
                            'codeLoc'    => __METHOD__,
                            'custMsg'    => '',
                            'retPack'    => array('valid_country_codes' => $this->myCountryCodes));
        } else
        {
            $result = array('errCode'    => 0,
                            'statusText' => 'Success',
                            'codeLoc'    => __METHOD__,
                            'custMsg'    => '',
                            'retPack'    => '');
        }
        $this->objLogger->debug('errCode: ' . $result['errCode'] . ' statusText: ' . $result['statusText'] . ' codeLoc: ' . __METHOD__ . ' custMsg: NA retPack: NA');

        return $result;
    }

    /**
     * This will validate the date with format yyyy{delimiter}mm{delimiter}dd
     * where {delimiter} is '/', '.', or '-'.
     *
     * @param string $myFieldName
     * @param string $myFieldValue
     *
     * @return array Keys: errCode, statusText, codeLoc, custMsg, retPack
     */
    public function validateDate(string $myFieldName,
                                 string $myFieldValue): array
    {
        $this->objLogger->debug(__METHOD__);

        $delimiter = substr($myFieldValue,
                            strlen($myFieldValue) - 3,
                            1);
        switch ($delimiter)
        {
            case '/':
            case '.':
            case '-':
                $explodedDate = explode($delimiter,
                                        $myFieldValue);
                if (is_numeric($explodedDate[0]) and is_numeric($explodedDate[1]) and is_numeric($explodedDate[2]))
                {
                    if (checkdate($explodedDate[1],
                                  $explodedDate[2],
                                  $explodedDate[0]))
                    {
                        $result = array('errCode'    => 0,
                                         'statusText' => 'Success',
                                         'codeLoc'    => __METHOD__,
                                         'custMsg'    => '',
                                         'retPack'    => '');
                    } else
                    {
                        $result = array('errCode'    => 400070,
                                         'statusText' => $myFieldName . ' (' . $myFieldValue . ') is not a valid date',
                                         'codeLoc'    => __METHOD__,
                                         'custMsg'    => '',
                                         'retPack'    => '');
                    }
                } else
                {
                    $result = array('errCode'    => 400000,
                                     'statusText' => $myFieldName . ' (' . $myFieldValue . ') contain none numeric values',
                                     'codeLoc'    => __METHOD__,
                                     'custMsg'    => '',
                                     'retPack'    => '');
                }
                break;
            default:
                $result = array('errCode'    => 200250,
                                 'statusText' => $myFieldName . ' (' . $myFieldValue . ') must contain one of these values: "/", ".", "-"',
                                 'codeLoc'    => __METHOD__,
                                 'custMsg'    => '',
                                 'retPack'    => '');
        }
        $this->objLogger->debug('errCode: ' . $result['errCode'] . ' statusText: ' . $result['statusText'] . ' codeLoc: ' . __METHOD__ . ' custMsg: NA retPack: NA');

        return $result;
    }

    /**
     * This will test if the supplied value has a valid email format
     *
     * @param string $myFieldName
     * @param string $myFieldValue
     *
     * @return array Keys: errCode, statusText, codeLoc, custMsg, retPack
     */
    public function validateEmailFormat(string $myFieldName,
                                        string $myFieldValue): array
    {
        $this->objLogger->debug(__METHOD__);
        if (preg_match_all('/^[\p{L}0-9-]{1,}.+@[\p{L}0-9-]+(\.[\p{L}0-9-]+)*(\.[\p{L}]{2,})$/mi',
                           $myFieldValue)) // This will test to see if email is empty and malformed. It will not test if the email exists.
        {
            $result = array('errCode'    => 0,
                            'statusText' => 'Success',
                            'codeLoc'    => __METHOD__,
                            'custMsg'    => '',
                            'retPack'    => '');
        } else
        {
            $result = array('errCode'    => 400050,
                            'statusText' => $myFieldName . ' (' . $myFieldValue . ')  is not a valid email address',
                            'codeLoc'    => __METHOD__,
                            'custMsg'    => '',
                            'retPack'    => '');
        }
        $this->objLogger->debug('errCode: ' . $result['errCode'] . ' statusText: ' . $result['statusText'] . ' codeLoc: ' . __METHOD__ . ' custMsg: NA retPack: NA');

        return $result;
    }

    /**
     * This will test to see if the pay period is valid.
     *
     * NOTE: The frequency that these values change is extremely limited, so it is hard coded.
     * NOTE: If the frequency of change increase, this should be migrated to a database table.
     *
     *
     * @param string $myFieldName
     * @param string $myFieldValue
     *
     * @return array Keys: errCode, statusText, codeLoc, custMsg, retPack
     *               retPack on an error will return special_characters key with the list of special characters
     */
    public function validatePayPeriod(string $myFieldName,
                                      string $myFieldValue): array
    {
        $this->objLogger->debug(__METHOD__);

        switch (strtoupper($myFieldValue))
        {
            case 'WEEKLY' :
            case 'TWO_WEEKS':
            case 'HALF_MONTH':
            case 'MONTHLY':
            case 'YEARLY':
                $result = array('errCode'    => 0,
                                'statusText' => 'Success',
                                'codeLoc'    => __METHOD__,
                                'custMsg'    => '',
                                'retPack'    => '');

                break;
            default:
                $result = array('errCode'    => 200250,
                                'statusText' => $myFieldName .
                                                ' (' .
                                                $myFieldValue .
                                                ') must contain one of these values: ' . $this->displayPayPeriods . ', YEARLY',
                                'codeLoc'    => __METHOD__,
                                'custMsg'    => '',
                                'retPack'    => '');
        }
        $this->objLogger->debug('errCode: ' . $result['errCode'] . ' statusText: ' . $result['statusText'] . ' codeLoc: ' . __METHOD__ . ' custMsg: NA retPack: NA');

        return $result;
    }

    /**
     * This will validate the timestamp
     *
     * @param string $myFieldName
     * @param string $myFieldValue Valid Format is 'Y-m-d G:i:s.u O'
     *
     * @return array Keys: errCode, statusText, codeLoc, custMsg, retPack
     */
    public function validateTimestamp(string $myFieldName,
                                      string $myFieldValue): array
    {
        $this->objLogger->debug(__METHOD__);

        try
        {
            $timestampResult = new \DateTime($myFieldValue);
            $result = array('errCode'    => 0,
                             'statusText' => 'Success',
                             'codeLoc'    => __METHOD__,
                             'custMsg'    => '',
                             'retPack'    => array('converttimestamp' => date_format($timestampResult,
                                                                                     'Y-m-d G:i:s.u O')));
        } catch (\Exception $e)
        {
            $result = array('errCode'    => 400080,
                             'statusText' => $myFieldName . ' (' . $myFieldValue . ') is not a valid timestamp. Format\'s are UTC, GMT or Zulu',
                             'codeLoc'    => __METHOD__,
                             'custMsg'    => '',
                             'retPack'    => '');
        }
        $this->objLogger->debug('errCode: ' . $result['errCode'] . ' statusText: ' . $result['statusText'] . ' codeLoc: ' . __METHOD__ . ' custMsg: NA retPack: NA');

        return $result;
    }

    /**
     * This will validate True or False condition
     *
     * @param string $myFieldValue
     *
     * @return string : FALSE or TRUE
     */
    public function validateTrueFalse(string $myFieldName,
                                      string $myFieldValue): array
    {
        $this->objLogger->debug(__METHOD__);
        if(strtoupper($myFieldValue) == 'TRUE')
        {
             $result = array('errCode'    => 0,
                                'statusText' => '',
                                'codeLoc'    => __METHOD__,
                                'custMsg'    => '',
                                'retPack'    => 'TRUE');

        }
        elseif(strtoupper($myFieldValue) == 'FALSE')
        {
            $result = array('errCode'    => 0,
                                'statusText' => '',
                                'codeLoc'    => __METHOD__,
                                'custMsg'    => '',
                                'retPack'    => 'FALSE');
        }
        else{
            $result = array('errCode'    => 200250,
                                'statusText' => $myFieldName.' ('.$myFieldValue.') must contain one of these values: true,True,TRUE,false,False,FALSE',
                                'codeLoc'    => __METHOD__,
                                'custMsg'    => '',
                                'retPack'    => '');
        }
        $this->objLogger->debug('errCode: ' . $result['errCode'] . ' statusText: ' . $result['statusText'] . ' codeLoc: ' . __METHOD__ . ' custMsg: NA retPack: NA');
        return $result;
    }

    /**
     * This will validate phone number
     *
     * @param string $myFieldValue
     * @param string $myFieldValue Valid Format is numeric size between 7 and 10
     * @return array Keys: errCode, statusText, codeLoc, custMsg, retPack
     */
    public function validatePhoneNumber(string $myFieldValue): array
    {
        $this->objLogger->debug(__METHOD__);
        $result = array('errCode'    => 0,
                'statusText' => 'Success',
                'codeLoc'    => __METHOD__,
                'custMsg'    => '',
                'retPack'    => '');
        if (is_numeric($myFieldValue)){
            if (strlen($myFieldValue) < 7)
            {
                $result = array('errCode'    => 400090,
                                'statusText' => 'Phone Number (' . $myFieldValue . ') is too small. Min size: 7 Actual size: ' . strlen($myFieldValue),
                                'codeLoc'    => __METHOD__,
                                'custMsg'    => '',
                                'retPack'    => '');

            }
            if (strlen($myFieldValue) > 10)
            {
                $result = array('errCode'    => 400090,
                                'statusText' => 'Phone Number (' . $myFieldValue . ') is too large. Max size: 10 Actual size: ' . strlen($myFieldValue),
                                'codeLoc'    => __METHOD__,
                                'custMsg'    => '',
                                'retPack'    => '');

            }        
        } 
        else
        {
            if (! is_null($myFieldValue))
            {
                $result = array('errCode'    => 400000,
                                'statusText' => 'phone_number (' . $myFieldValue . ') is not numeric',
                                'codeLoc'    => __METHOD__,
                                'custMsg'    => '',
                                'retPack'    => '');

            }
        }
        $this->objLogger->debug('errCode: ' . $result['errCode'] . ' statusText: ' . $result['statusText'] . ' codeLoc: ' . __METHOD__ . ' custMsg: NA retPack: NA');
        return $result;

    }
   
    /**
     * contains Alpha Undersore Chars will test if the following characters are only alpha(case insensitive) and undescore
     * 
     * @param string $myFieldName
     * @param string $myFieldValue
     *
     * @return array Keys: errCode, statusText, codeLoc, custMsg, retPack
     *               retPack on an error will return allowed_characters (key) with the *A-Za-z_* regex (value)
     */
    public function containsAlphaUnderscoreChars(string $myFieldName,
                                              string $myFieldValue): array
    {
        $this->objLogger->debug(__METHOD__);

        if (preg_match_all('/^[a-zA-Z_]+$/m',
                           $myFieldValue))
        {
            $result = array('errCode'    => 0,
                            'statusText' => 'Success',
                            'codeLoc'    => __METHOD__,
                            'custMsg'    => '',
                            'retPack'    => '');
        } 
        else
        {
            $result = array('errCode'    => 400065,
                            'statusText' => $myFieldName . ' (' . $myFieldValue . ') contains special characters other than alpha and underscore',
                            'codeLoc'    => __METHOD__,
                            'custMsg'    => '',
                            'retPack'    => '');
        }
        $this->objLogger->debug('errCode: ' . $result['errCode'] . ' statusText: ' . $result['statusText'] . ' codeLoc: ' . __METHOD__ . ' custMsg: NA retPack: NA');

        return $result;
    }
    /**
     * contains Alpha Space Characters will test if the following characters are only alpha(case insensitive) and space
     * 
     * @param string $myFieldName
     * @param string $myFieldValue
     *
     * @return array Keys: errCode, statusText, codeLoc, custMsg, retPack
     *               retPack on an error will return allowed_characters (key) with the *A-Za-z\s* regex (value)
     */
    public function containsAlphaSpaceChars(string $myFieldName,
                                              string $myFieldValue): array
    {
        $this->objLogger->debug(__METHOD__);

        if (preg_match_all('/^[a-zA-Z\s]+$/m',
                           $myFieldValue))
        {
            $result = array('errCode'    => 0,
                            'statusText' => 'Success',
                            'codeLoc'    => __METHOD__,
                            'custMsg'    => '',
                            'retPack'    => '');
        } 
        else
        {
            $result = array('errCode'    => 400065,
                            'statusText' => $myFieldName . ' (' . $myFieldValue . ') contains special characters other than alpha and space',
                            'codeLoc'    => __METHOD__,
                            'custMsg'    => '',
                            'retPack'    => '');
        }
        $this->objLogger->debug('errCode: ' . $result['errCode'] . ' statusText: ' . $result['statusText'] . ' codeLoc: ' . __METHOD__ . ' custMsg: NA retPack: NA');

        return $result;
    }
     /**
     * contains Alpha Space Comma Characters will test if the following characters are only alpha(case insensitive) space and comma
     * 
     * @param string $myFieldName
     * @param string $myFieldValue
     *
     * @return array Keys: errCode, statusText, codeLoc, custMsg, retPack
     *               retPack on an error will return allowed_characters (key) with the *A-Za-z\s* regex (value)
     */
    public function containsAlphaSpaceCommaChars(string $myFieldName,
                                                 string $myFieldValue): array
    {
        $this->objLogger->debug(__METHOD__);

        if (preg_match_all('/^[a-zA-Z\s,]+$/m',
                           $myFieldValue))
        {
            $result = array('errCode'    => 0,
                            'statusText' => 'Success',
                            'codeLoc'    => __METHOD__,
                            'custMsg'    => '',
                            'retPack'    => '');
        } 
        else
        {
            $result = array('errCode'    => 400065,
                            'statusText' => $myFieldName . ' (' . $myFieldValue . ') contains special characters other than alpha, comma and space',
                            'codeLoc'    => __METHOD__,
                            'custMsg'    => '',
                            'retPack'    => '');
        }
        $this->objLogger->debug('errCode: ' . $result['errCode'] . ' statusText: ' . $result['statusText'] . ' codeLoc: ' . __METHOD__ . ' custMsg: NA retPack: NA');

        return $result;
    }
    /**
     * adds a preceding + to country code if it's missing
     * 
     * @param string $myFieldName
     * @param string $myFieldValue
     *
     * @return array Keys: errCode, statusText, codeLoc, custMsg, retPack
     *               retPack returns the edited country code
     */
    public function formatCountryCode(string $myFieldName,
                                              string $myFieldValue): array
    {
    $this->objLogger->debug(__METHOD__);

    if (substr($myFieldValue,
               0,
               1) <> '+'){
    
        $myFieldValue = '+' . $myFieldValue;
    }
    
   $result = array('errCode'    => 0,
                    'statusText' => 'Success',
                    'codeLoc'    => __METHOD__,
                    'custMsg'    => '',
                    'retPack'    => array($myFieldName => $myFieldValue));

   $this->objLogger->debug('errCode: ' . $result['errCode'] . ' statusText: ' . $result['statusText'] . ' codeLoc: ' . __METHOD__ . ' custMsg: NA retPack: NA');

        return $result;
            
        
    }

    /**
     * This will provide a list of valid Payment Methods
     *
     * NOTE: The frequency that these values change is extremely limited, so it is hard coded.
     * NOTE: If the frequency of change increase, this should be migrated to a database table.
     *
     *
     * @return array Keys: errCode, statusText, codeLoc, custMsg, retPack
     */

    public function listPaymentMethods(): array
    {
        $this->objLogger->debug(__METHOD__);

        $this->objLogger->debug('errCode: 0 statusText: Success codeLoc: ' . __METHOD__ . ' custMsg: NA retPack: NA');
        $result = array('errCode'    => 0,
                        'statusText' => 'Success',
                        'codeLoc'    => __METHOD__,
                        'custMsg'    => '',
                        'retPack'    => array('lower_case' => array('values'      => array('mpesa',
                                                                                             'credit'),
                                                                    'display_value' => array('mpesa',
                                                                                             'credit (lpo)')),
                                              'mixed_case' => array('values'      => array('Mpesa',
                                                                                             'Credit'),
                                                                    'display_value' => array('Mpesa',
                                                                                             'Credit (LPO)')),
                                              'upper_case' => array('values'      => array('MPESA',
                                                                                             'CREDIT'),
                                                                    'display_value' => array('MPESA',
                                                                                             'CREDIT (LPO)'))));
        $this->objLogger->debug('errCode: ' . $result['errCode'] . ' statusText: ' . $result['statusText'] . ' codeLoc: ' . __METHOD__ . ' custMsg: NA retPack: NA');

        return $result;
            
        
    }

    /**
     * This will test to see if the payment methods is valid.
     *
     * NOTE: The frequency that these values change is extremely limited, so it is hard coded.
     * NOTE: If the frequency of change increase, this should be migrated to a database table.
     *
     *
     * @param string $myFieldName
     * @param string $myFieldValue
     *
     * @return array Keys: errCode, statusText, codeLoc, custMsg, retPack
     *               retPack on an error will return special_characters key with the list of special characters
     */
    public function validatePaymentMethods(string $myFieldName,
                                      string $myFieldValue): array
    {
        $this->objLogger->debug(__METHOD__);

        switch (strtoupper($myFieldValue))
        {
            case 'MPESA' :
            case 'CREDIT':
                $result = array('errCode'    => 0,
                                'statusText' => 'Success',
                                'codeLoc'    => __METHOD__,
                                'custMsg'    => '',
                                'retPack'    => '');

                break;
            default:
                $result = array('errCode'    => 200250,
                                'statusText' => $myFieldName .
                                                ' (' .
                                                $myFieldValue .
                                                ') must contain one of these values: ' . $this->displayPaymentMethods,
                                'codeLoc'    => __METHOD__,
                                'custMsg'    => '',
                                'retPack'    => '');
        }
        $this->objLogger->debug('errCode: ' . $result['errCode'] . ' statusText: ' . $result['statusText'] . ' codeLoc: ' . __METHOD__ . ' custMsg: NA retPack: NA');

        return $result;
    } 

    /**
     * This will provide a list of valid international currency (ISO-4217)
     *
     * NOTE: The frequency that these values change is extremely limited, so it is hard coded.
     * NOTE: If the frequency of change increase, this should be migrated to a database table.
     *
     *
     * @param string $myFieldName
     * @param string $myFieldValue
     *
     * @return array Keys: errCode, statusText, codeLoc, custMsg, retPack
     *               retPack on an error will return valid_currency_type (key) with the list of country codes (value)
     */
    public function validateCurrencyType(string $myFieldName,
                                         string $myFieldValue): array
    {
        $this->objLogger->debug(__METHOD__);

        $searchResult = array_search($myFieldValue,
                                     $this->myCurrenctTypes);

        if ($searchResult === FALSE)
        {
            $result = array('errCode'    => 200250,
                            'statusText' => $myFieldName . ' (' . $myFieldValue . ') must contain one of these values: ' . json_encode($this->myCurrenctTypes),
                            'codeLoc'    => __METHOD__,
                            'custMsg'    => '',
                            'retPack'    => array('valid_currency_type' => $this->myCurrenctTypes));
        } else
        {
            $result = array('errCode'    => 0,
                            'statusText' => 'Success',
                            'codeLoc'    => __METHOD__,
                            'custMsg'    => '',
                            'retPack'    => '');
        }
        $this->objLogger->debug('errCode: ' . $result['errCode'] . ' statusText: ' . $result['statusText'] . ' codeLoc: ' . __METHOD__ . ' custMsg: NA retPack: NA');

        return $result;
    }   
    /**
     * Class Constructor
     *
     * @param $logger
     */
    public function __construct($logger)
    {
        $this->objLogger = $logger;
        $this->objLogger->debug(__METHOD__);

        $this->myCountryCodes = require __DIR__ . '/../../CountryCodeData.php';
        $this->myCurrenctTypes = require __DIR__ . '/../../CurrencyTypeData.php';

    }
}
