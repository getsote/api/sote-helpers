<?php
/**
 * Helper class for Database
 *
 * These methods are used any where in the system that database activities occur
 *
 */

/**
 * Created by PhpStorm.
 * User: syacko
 * Date: 7/16/18
 * Time: 9:10 AM
 * Author:    Scott
 * Co-author: Mary
 */

namespace HELPERS;

use Doctrine\DBAL\DBALException;
use Doctrine\DBAL\Types\StringType;
use Doctrine\DBAL\Types\IntegerType;

/**
 * Class DBHelper
 */
class DBHelper
{
    /**
     * @var object $db The instance of the Doctrine DBAL
     */
    protected $objDB;
    /**
     * @var object $logger The instance of the "Slim\Http\RequestMonolog\Logger" created at startup.
     */
    protected $objLogger;

    /**
     * This will validate that the row version supplied matches the row_version in the database. It will place a "FOR SHARE" lock on the row in the table.
     *
     * @param string $myTableName  The name of the table that is going to be updated.
     * @param int    $myRowKey     The system generated row identifier for each row of the table. Examples are organizations_id, usermanagement_id.
     * @param int    $myRowVersion This is the value of the row_version for the row identitied by $myRowKey
     *
     * @return array Keys: errCode, statusText, codeLoc, custMsg, retPack
     */
    public function validateRowVersion($myTableName,
                                       $myRowKey,
                                       $myRowVersion): array
    {
        $this->objLogger->debug(__METHOD__);

        try
        {
            $sql       = 'SELECT iscol.column_name FROM information_schema.columns iscol ' .
                         'INNER JOIN information_schema.key_column_usage iskcu ' .
                         'ON iscol.table_schema = iskcu.table_schema ' .
                         'AND iscol.table_name = iskcu.table_name ' .
                         'WHERE iscol.table_schema = \'sote\' ' .
                         'AND iscol.table_name = :tblName ' .
                         'AND iscol.column_name = iskcu.column_name';  // Verify the table/column exists
            $sqlResult = $this->objDB->fetchArray($sql,
                                                  array('tblName' => $myTableName,
                                                        StringType::STRING));
            if ($sqlResult[0])
            {
                $sql       = 'SELECT row_version FROM sote.' . $myTableName .
                             ' WHERE ' . $sqlResult[0] . ' = :myRowKey ' .
                             'AND row_version = :rowVersion ' .
                             'FOR SHARE';  // Verify the table/column key value row version matches
                $sqlResult = $this->objDB->fetchColumn($sql,
                                                       array("myRowKey"   => $myRowKey,
                                                             IntegerType::INTEGER,
                                                             "rowVersion" => $myRowVersion,
                                                             IntegerType::INTEGER));
                if ($sqlResult == 0)
                {
                    $result = array('errCode'    => 200000,
                                    'statusText' => 'Row has been updated since reading it, re-read the row',
                                    'codeLoc'    => __METHOD__,
                                    'custMsg'    => '',
                                    'retPack'    => '');
                } else
                {
                    $result = array('errCode'    => 0,
                                    'statusText' => 'Success',
                                    'codeLoc'    => __METHOD__,
                                    'custMsg'    => '',
                                    'retPack'    => '');
                }
            } else
            {
                $result = array('errCode'    => 200100,
                                'statusText' => 'Table doesn\'t exist',
                                'codeLoc'    => __METHOD__,
                                'custMsg'    => '',
                                'retPack'    => '');
            }
        } catch (DBALException $e1)
        {
            $result = array('errCode'    => 209999,
                            'statusText' => 'SQL error - see details in retPack',
                            'codeLoc'    => __METHOD__,
                            'custMsg'    => '',
                            'retPack'    => array('sqlstate'   => $e1->getSQLState(),
                                                  'sqlmessage' => $e1->getMessage()));
            $this->objLogger->emergency('errCode: ' . $result['errCode'] . ' statusText: ' . $result['statusText'] . ' codeLoc: ' . __METHOD__ . ' custMsg: NA retPack: sqlstate: ' . $e1->getSQLState() . ' sqlmessage: ' .
                                        $e1->getMessage());
        }
        $this->objLogger->debug('errCode: ' . $result['errCode'] . ' statusText: ' . $result['statusText'] . ' codeLoc: ' . __METHOD__ . ' custMsg: NA retPack: NA');

        return $result;
    }
    /**
     * formats formats Json Array String / json object 
     * 
     * @param string $myFieldValue
     *
     * @return array Keys: errCode, statusText, codeLoc, custMsg, retPack
     *               retPack returns the formatted value
     */
    public function formatSQLArrayString(string $myFieldValue): array
    {
        $myFormattedValue = str_replace('[',
                                        '{',
                                        str_replace(']',
                                                    '}',
                                                    str_replace('"',
                                                                '',
                                                                $myFieldValue)));
        $result = array('errCode'    => 0,
                        'statusText' => 'Success',
                        'codeLoc'    => __METHOD__,
                        'custMsg'    => '',
                        'retPack'    => $myFormattedValue);

        $this->objLogger->debug('errCode: ' . $result['errCode'] . ' statusText: ' . $result['statusText'] . ' codeLoc: ' . __METHOD__ . ' custMsg: NA retPack: NA');

        return $result;
    }
    /**
     * DBHelper constructor.
     *
     * @param object $objLogger
     * @param object $objDB
     *
     */
    public function __construct($logger,
                                $db)
    {
        $this->objLogger = $logger;
        $this->objLogger->debug(__METHOD__);

        $this->objDB = $db;
    }
}